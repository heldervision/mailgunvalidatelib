<?php


namespace mailValidator;


class ValidateMail
{
    private $apiKey;
    private $errorArray = array('unknown_provider', 'no_mx', 'mailbox_does_not_exist', 'mailbox_is_disposable_address', 'smtp_error', 'malformed address; missing @ sign', 'No MX records found for domain');
    private $errorMessage = array('Onbekende provider', 'Geen mailserver gevonden', 'Maibox bestaat niet', 'Mailbox is een tijdelijk adres', 'Adres klopt niet', 'Email adres heeft geen @', 'Domein naam klopt niet');

    //Set apiKey fot this class
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function validate($email){
        $response = $this->validationRequest($email);

        //Look if the response is good
        return $this->buildResponse(json_decode($response,true));
    }

    private function validationRequest($email){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $this->apiKey);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_POSTFIELDS, array("address" => $email));
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v4/address/validate');
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    private function buildResponse($response){
        if(isset($response['message'])) {
            return array('validated' => null, 'did_you_mean' => null, 'reason' => 'API key niet goed');
        }elseif($this->checkErrorMessages($response)['result']){ //Check if we know the reason
            $error = $this->checkErrorMessages($response);
            return array('validated' => false, 'did_you_mean' => isset($response['did_you_mean']) ? $response['did_you_mean'] : null, 'reason' => $error['response']);
        }elseif($response['result'] == 'deliverable'){
            return array('validated' => true, 'did_you_mean' => null, 'reason' => null);
        }else{ //TODO: Dit overlegen met Wiljo hoe hij dit precies wil doen
            return array('validated' => true, 'did_you_mean' => null, 'reason' => null);
        }
    }

    private function checkErrorMessages($response){
        // Because the error order handling we have to check if the address isnt deliverable
        if($response['result'] != 'deliverable') {
            foreach ($this->errorArray as $key => $message) {
                if (stripos($response['reason'][0], $message) !== false) {
                    return array('result' => 1, 'key' => $key, 'response' => $this->errorMessage[$key]);
                } elseif ($message == $response['reason'][0]) {
                    return array('result' => 1, 'key' => $key, 'response' => $this->errorMessage[$key]);
                }
            }
        }else{
            return array('result' => false);
        }
        return array('result' => false);
    }
};