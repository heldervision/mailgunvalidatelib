<?php

namespace Validator;
require_once 'ValidateMail.php';

use mailValidator\ValidateMail;

class main
{
    protected $apikey;
    public $validatorMail;

    //This is used to set the apikey var and give back the validation function
    public function __construct($apikey)
    {
        $this->apikey = $apikey;
        $this->validatorMail = new ValidateMail($apikey);
    }

    public static function create($apiKey)
    {
       return new self($apiKey);
    }
}
